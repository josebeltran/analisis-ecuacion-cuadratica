#ifndef GRAFICOS_H
#define GRAFICOS_H

#include <QMainWindow>
#include <QtCharts>
#include <iostream>

using namespace QtCharts;
namespace Ui {
class Graficos;
}

class Graficos : public QMainWindow
{
    Q_OBJECT

public:
    explicit Graficos(QWidget *parent = 0);
    ~Graficos();



private:
    Ui::Graficos *ui;
};

#endif // GRAFICOS_H

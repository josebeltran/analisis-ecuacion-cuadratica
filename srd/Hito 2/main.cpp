#include "graficos.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Graficos w;

    /*creamos las variables necesarios. res1,res2 y res3 serán las
     * respuestas que ingresará el usuario para luego reemplazar en la ecuación.
     * x1 será la variable del eje x e irá desde el -100 hasta el 100 para asi asegurar presición.
     * y1 será reemplaza con la ecuación cuadrática y los valores ingresados por le usuario.
     */
    double res1,res2,res3,x1,y1;
    std::cout<<"Ingrese valor de a: "; std::cin>>res1;
    std::cout<<"Ingrese valor de b: "; std::cin>>res2;
    std::cout<<"Ingrese valor de c: "; std::cin>>res3;

    QLineSeries *line = new QLineSeries();
    QLineSeries *line2 = new QLineSeries();
    QLineSeries *line3 = new QLineSeries();
    /* ciclo for para crear los puntos de la gráfica, el cual
     * dependerá de la presición, en este caso x1 e [-100,100]
     * por cada x1, hará un y1 reemplando las variables y luego
     * los irá agregando a la linea.
    */
    for(x1=-100;x1<=100;x1++){
        y1 = (res1*x1*x1+res2*x1+res3);
        line->append(x1,y1);
    }

    //ciclo for para crear el eje x
    for(x1=-100;x1<=100;x1++){
        line2->append(x1,0);
    }
    //ciclo for para crear el eje y
    for(x1=-100;x1<=100;x1++){
        line3->append(0,x1);
    }

    QChart *chart = new QChart();

    line->setName("Función Cuadrática ax²+bx+c");
    line2->setName("Eje x");
    line3->setName("Eje y");

    chart->addSeries(line);
    chart->addSeries(line2);
    chart->addSeries(line3);
    chart->createDefaultAxes();
    chart->setTitle("Gráfica función");

    /*
    *Las dos lineas de abajo sirven para poner
    *valores a los ejes y asi no tiendan a números
    *tan grandes que no nos entregan información útil
    */
    chart->axisX()->setRange(-40,40);
    chart->axisY()->setRange(-40,40);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    w.setCentralWidget(chartView);

    w.show();

    return a.exec();
}





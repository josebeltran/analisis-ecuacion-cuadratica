# Análisis Ecuación cuadrática

Descripción Proyecto: Con los coeficientes de una función cuadrática; expresar raíces, vértice, concavidad y corte con el eje de las ordenas de la ecuación cuadrática de manera grafica. Se crea una interfaz gráfica de usuario mostrando La función, a través de un plano cartesiano con el framework Qt. 

Requisitos de compilación o plataforma: 
los requisitos de hardware es que el computador cuente y corra de forma óptima con un compilador de C++,
El sistema puede ser ejecutado en la terminal del sistema operativo.

¿Cómo instalar o compilar?
Se puede compilar con algún programa que contenga compilador C++, de preferencia compilar en alguna distribución de linux, en la terminal con comando (g++ -o) o bien con make.

Integrantes (nombre-rol): José Beltrán Mobarec-202030548-k, Fabian Miranda-202030515-3, Maximiliano Pozo-201930536-0 . 
--------------------------------------------------------------------------------------------------------------------------------------------------
Hito 2.
Descripción de hito 2: En esta etapa del proyecto, se busca poder realizar una grafica respectiva a lo que seria nuestro topico del proyecto, "Ecuación cuadratica", buscando que tenga una entrada de datos mediante la consola, y como consecuencia de esto, la exposición del grafico respectivo a los datos entregados anteriormente. 
--------------------------------------------------------------------------------------------------------------------------------------------------
Hito 3.
Descripción de hito 3: En constraste a los 2 hitos anteriores, el Hito 3 tiene que ser capaz de demostrar un uso eficaz del codigo, mediante la interfaz grafica, dandole la capacidad absoluta al usuario, de poder manejar de manera simplificada el programa, con el principal fin de darle el uso que desde un inicio fue predeterminado, poder graficar y manipular la grafica en cuestión. 

Posible error al ejecutar:
    Al ejecutar en qt sale una atención; "tiene una hora de modificación XXXXs en el futuro", esta se soluciona haciendo un cambio de hora, particularmente adelantandola. 

Hay dos archivos bastante extensos, los cuales no los creamos nosotros, los demás si creamos a bases de ayudas y guías.
